package ru.leonidivankin.popularlibrary1adapter.recycler.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.List;

import ru.leonidivankin.popularlibrary1adapter.recycler.model.Data;
import ru.leonidivankin.popularlibrary1adapter.recycler.view.IViewHolder;
import ru.leonidivankin.popularlibrary1adapter.recycler.view.ThreeView;

@InjectViewState
public class ThreePresenter extends MvpPresenter<ThreeView> {

    RecyclerThreePresenter recyclerMainPresenter = new RecyclerThreePresenter();

    private class RecyclerThreePresenter implements IRecyclerThreePresenter {

        private Data data = new Data();
        private List<String> list = data.getList();

        @Override
        public void bindView(IViewHolder holder) {
            holder.setText(list.get(holder.getPos()));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }

    public RecyclerThreePresenter getRecyclerMainPresenter() {
        return recyclerMainPresenter;
    }
}
