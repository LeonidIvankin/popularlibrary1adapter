package ru.leonidivankin.popularlibrary1adapter.recycler.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.leonidivankin.popularlibrary1adapter.R;
import ru.leonidivankin.popularlibrary1adapter.recycler.presenter.IRecyclerThreePresenter;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private IRecyclerThreePresenter iRecyclerMainPresenter;

    public MyAdapter(IRecyclerThreePresenter iRecyclerMainPresenter) {
        this.iRecyclerMainPresenter = iRecyclerMainPresenter;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_three, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.position = position;
        iRecyclerMainPresenter.bindView(holder);
    }

    @Override
    public int getItemCount() {
        return iRecyclerMainPresenter.getItemCount();
    }



    class MyViewHolder extends RecyclerView.ViewHolder implements IViewHolder {

        private TextView textView;
        private int position = 0;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.text_view);
        }

        @Override
        public void setText(String text) {
            textView.setText(text);
        }

        @Override
        public int getPos() {
            return position;
        }


    }
}
