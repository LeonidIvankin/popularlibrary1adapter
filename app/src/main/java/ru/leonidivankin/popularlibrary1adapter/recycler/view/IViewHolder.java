package ru.leonidivankin.popularlibrary1adapter.recycler.view;

public interface IViewHolder {
    void setText(String text);
    int getPos();
}
