package ru.leonidivankin.popularlibrary1adapter.recycler.presenter;


import ru.leonidivankin.popularlibrary1adapter.recycler.view.IViewHolder;

public interface IRecyclerThreePresenter {
    void bindView(IViewHolder iViewHolder);
    int getItemCount();
}
